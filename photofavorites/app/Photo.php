<?php

declare(strict_types=1);

namespace App;

use Jenssegers\Model\Model as BaseModel;

class Photo extends BaseModel
{
    /** @var array */
    protected $guarded = [];
}
