<?php

declare(strict_types=1);

namespace App\Repositories;

use App\Photo;
use Jenssegers\Model\Model as BaseModel;
use Illuminate\Support\Collection;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use App\Services\HttpJsonFetcherService;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Support\Facades\Cache;

class PhotoRepository implements RepositoryInterface
{
    /** @var int */
    const CACHE_TIME_SECONDS = 10*60;

    /** @var array */
    const DIRECT_MAPPED_ATTRIBUTES = [
        'id',
        'albumId',
        'title',
        'url',
        'thumbnailUrl',
    ];

    /** @var HttpJsonFetcherService */
    private $httpFetcher;

    public function __construct(HttpJsonFetcherService $httpFetcher)
    {
        $this->httpFetcher = $httpFetcher;
    }

    /**
     * @return Photo|null
     * @throws GuzzleException
     */
    public function find(int $id): ?BaseModel
    {
        $httpFetcher = $this->httpFetcher;
        $photoFetch = Cache::remember("photos-{$id}", self::CACHE_TIME_SECONDS, function () use ($httpFetcher, $id) {
            return $httpFetcher->fetch("photos/{$id}");
        });

        return $photoFetch ? $this->mapApiResultToModel($photoFetch['data'][0]) : null;
    }

    /**
     * @throws GuzzleException
     */
    public function all(): Collection
    {
        $photoFetch = $this->httpFetcher->fetch('photos');

        return $photoFetch ? collect(array_map([$this, 'mapApiResultToModel'], $photoFetch['data'])) : collect();
    }

    /**
     * @throws GuzzleException
     */
    public function paginate(int $page): LengthAwarePaginator
    {
        $httpFetcher = $this->httpFetcher;
        $photoFetch = Cache::remember("photos-page-{$page}", self::CACHE_TIME_SECONDS, function () use ($httpFetcher, $page) {
            return $httpFetcher->fetch('photos', $page);
        });

        if (!$photoFetch) {
            return new LengthAwarePaginator(
                null,
                0,
                1,
                1
            );
        }

        $photos = collect(array_map([$this, 'mapApiResultToModel'], $photoFetch['data']));

        return new LengthAwarePaginator(
            $photos,
            $photoFetch['totalItems'],
            $photoFetch['pageSize'],
            $page,
            [
                'path' => Paginator::resolveCurrentPath()
            ]
        );
    }

    /**
     * @param mixed $item
     * @return Photo
     */
    private function mapApiResultToModel($item): Photo
    {
        $mappedData = [];

        foreach (self::DIRECT_MAPPED_ATTRIBUTES as $property) {
            $mappedData[$property] = $item->$property;
        }

        return new Photo($mappedData);
    }
}
