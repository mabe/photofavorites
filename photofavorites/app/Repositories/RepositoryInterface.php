<?php

declare(strict_types=1);

namespace App\Repositories;

use Jenssegers\Model\Model as BaseModel;
use Illuminate\Support\Collection;
use Illuminate\Pagination\LengthAwarePaginator;

interface RepositoryInterface
{
    /**
     * Return an exact item referenced by id or null if not found.
     *
     * @param int $id
     * @return BaseModel|null
     */
    public function find(int $id): ?BaseModel;

    /**
     * Return all items.
     *
     * @return Collection
     */
    public function all(): Collection;

    /**
     * Return a paginated subset of items.
     *
     * @param int $page
     * @return LengthAwarePaginator
     */
    public function paginate(int $page): LengthAwarePaginator;
}
