<?php

declare(strict_types=1);

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Repositories\PhotoRepository;
use Illuminate\Database\Eloquent\Relations\Relation;

class Favorite extends Model
{
    /** @var array */
    protected $fillable = [
        'photo_id',
        'user_id',
    ];

    /**
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getPhoto(): ?Photo
    {
        /** @var PhotoRepository $photoRepository */
        $photoRepository = app('PhotoRepository');

        return $photoRepository->find($this->photo_id);
    }

    public function user(): Relation
    {
        return $this->belongsTo(User::class);
    }
}
