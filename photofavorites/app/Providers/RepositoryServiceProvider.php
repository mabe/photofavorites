<?php

declare(strict_types=1);

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    public function register(): void
    {
        $this->app->singleton(
            'jsonplaceholder.HttpJsonFetcher',
            function () {
                return new \App\Services\HttpJsonFetcherService(
                    app(\GuzzleHttp\Client::class),
                    'http://jsonplaceholder.typicode.com/',
                    12,
                    '_page',
                    '_limit'
                );
            }
        );

        $this->app->singleton(
            'PhotoRepository',
            function () {
                return new \App\Repositories\PhotoRepository(
                    app('jsonplaceholder.HttpJsonFetcher')
                );
            }
        );
    }

    public function boot(): void
    {
        //
    }
}
