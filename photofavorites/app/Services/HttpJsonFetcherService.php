<?php

declare(strict_types=1);

namespace App\Services;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Psr7\UriResolver;
use function GuzzleHttp\Psr7\uri_for;

class HttpJsonFetcherService
{
    /** @var Client */
    private $httpClient;

    /** @var string */
    private $baseurl;

    /** @var int */
    private $pageSize;

    /** @var string */
    private $pageQueryName;

    /** @var string */
    private $pageSizeQueryName;

    public function __construct(Client $httpClient, string $baseurl, int $pageSize, string $pageQueryName, string $pageSizeQueryName)
    {
        $this->httpClient = $httpClient;
        $this->baseurl = $baseurl;
        $this->pageSize = $pageSize;
        $this->pageQueryName = $pageQueryName;
        $this->pageSizeQueryName = $pageSizeQueryName;
    }

    /**
     * @throws GuzzleException
     */
    public function fetch(string $endpoint, int $page=null): ?array
    {
        $uri = UriResolver::resolve(uri_for($this->baseurl), uri_for($endpoint));

        $query = [];

        if ($page) {
            $query[$this->pageQueryName] = $page;
            $query[$this->pageSizeQueryName] = $this->pageSize;
        }

        /* call api, if item not found -> return null */
        try {
            $response = $this->httpClient->request('GET', $uri, ['query' => $query]);
        } catch (GuzzleException $e) {
            if ($e->getCode() === 404) {
                return null;
            } else {
                throw $e;
            }
        }

        /* decode json to data-object, make sure it is in an array */
        $item_or_list = json_decode($response->getBody()->getContents());
        $data = is_array($item_or_list) ? $item_or_list : [$item_or_list];

        $result = ['data' => $data];

        if ($page) {
            $result['totalItems'] = (int) $response->getHeader('X-Total-Count')[0];

            $result['totalPages'] = (int) ceil($result['totalItems'] / $this->pageSize);

            $result['pageSize'] = $this->pageSize;
        }

        return $result;
    }
}
