<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\View\View;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Auth;
use App\Favorite;
use App\User;
use Carbon\CarbonImmutable;
use Illuminate\Database\Eloquent\Builder;
use stdClass;

class FavoriteController extends Controller
{
    const TOP_COUNT = 6;

    public function index(): View
    {
        $now = CarbonImmutable::now(config('app.timezone'));

        if ($now->isWeekend()) {
            return $this->index_topFavorizers();
        } else {
            return $this->index_latestFavorized();
        }
    }

    private function index_latestFavorized(): View
    {
        $favorites = Favorite::latest()->take(self::TOP_COUNT)->with('user')->get();

        return view('favorites.latest-favorized', compact('favorites'));
    }

    private function index_topFavorizers(): View
    {
        $weekStart = CarbonImmutable::
            now(config('app.timezone'))
            ->startOfWeek()
            ->toDateTimeString();

        $dateFilter = function (Builder $query) use ($weekStart) {
            $query->where('created_at', '>=', $weekStart);
        };

        $users = User::
            withCount(['favorites' => $dateFilter])
            ->whereHas('favorites', $dateFilter, '>', 0)
            ->orderBy('favorites_count', 'desc')
            ->orderBy('name', 'asc')
            ->take(self::TOP_COUNT)
            ->get();

        return view('favorites.top-favorizers', compact('users'));
    }

    public function create(): JsonResponse
    {
        abort(404);
        return response()->json(new stdClass());
    }

    public function store(Request $request): RedirectResponse
    {
        $validatedData = $request->validate([
            'photoId' => 'required|integer|min:1'
        ]);

        Favorite::firstOrCreate([
            'photo_id' => $validatedData['photoId'],
            'user_id' => Auth::id(),
        ]);

        return back();
    }

    public function show(Favorite $favorite): JsonResponse
    {
        abort(404);
        return response()->json(new stdClass());
    }

    public function edit(Favorite $favorite): JsonResponse
    {
        abort(404);
        return response()->json(new stdClass());
    }

    public function update(Request $request, Favorite $favorite): JsonResponse
    {
        abort(404);
        return response()->json(new stdClass());
    }

    public function destroy(Favorite $favorite): RedirectResponse
    {
        $favorite->delete();
        return back();
    }
}
