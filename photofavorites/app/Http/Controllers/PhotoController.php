<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\View\View;
use Illuminate\Http\JsonResponse;
use Auth;
use App\Photo;
use App\Favorite;
use App\Repositories\RepositoryInterface;
use Illuminate\Pagination\Paginator;
use GuzzleHttp\Exception\GuzzleException;
use stdClass;

class PhotoController extends Controller
{
    /** @var RepositoryInterface */
    protected $photoRepository;

    public function __construct()
    {
        $this->photoRepository = app('PhotoRepository');
    }

    /**
     * @throws GuzzleException
     */
    public function index(): View
    {
        $photos = $this->photoRepository->paginate(Paginator::resolveCurrentPage());

        $photoIds = $photos->pluck('id');
        $favorites = Favorite::whereUserId(Auth::id())->whereIn('photo_id', $photoIds)->get();

        $photos->map(function (Photo $photo) use ($favorites) {
            $favorite = $favorites->where('photo_id', $photo->id)->first();
            $photo->favoriteId = $favorite->id ?? null;
        });

        return view('photos.index', compact('photos'));
    }

    public function create(): JsonResponse
    {
        abort(404);
        return response()->json(new stdClass());
    }

    public function store(Request $request): JsonResponse
    {
        abort(404);
        return response()->json(new stdClass());
    }

    /**
     * @throws GuzzleException
     */
    public function show(int $id): View
    {
        $photo = $this->photoRepository->find($id);

        if (!$photo) {
            abort(404);
        }

        $favorite = Favorite::whereUserId(Auth::id())->where('photo_id', $photo->id)->first();

        $photo->favoriteId = $favorite->id ?? null;

        return view('photos.show', compact('photo'));
    }

    public function edit(int $id): JsonResponse
    {
        abort(404);
        return response()->json(new stdClass());
    }

    public function update(Request $request, int $id): JsonResponse
    {
        abort(404);
        return response()->json(new stdClass());
    }

    public function destroy(int $id): JsonResponse
    {
        abort(404);
        return response()->json(new stdClass());
    }
}
