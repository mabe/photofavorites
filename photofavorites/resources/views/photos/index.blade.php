@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="float-left">
                    <h2>Photos</h2>
                </div>
                <div class="float-right">
                    page {{ $photos->currentPage() }} of {{ $photos->lastPage() }}
                </div>
            </div>
        </div>

        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card-columns">
                    @foreach($photos as $photo)
                        <div class="card border-primary p-1" style="width:150px;">
                            <a href="{{ route('photos.show', $photo->id) }}">
                                <img src="{{ $photo->thumbnailUrl }}" class="card-img-top" alt="{{ $photo->id }}">
                            </a>
                            <div class="float-left small">{{ $photo->id }}</div>
                            <div class="float-right">
                                @php
                                    if ($photo->favoriteId) {
                                        $action = action([\App\Http\Controllers\FavoriteController::class, 'destroy'], ['id' => $photo->favoriteId]);
                                        $method = 'DELETE';
                                    } else {
                                        $action = action([\App\Http\Controllers\FavoriteController::class, 'store']);
                                        $method = 'POST';
                                    }
                                @endphp

                                <form method="POST" action="{{ $action }}">
                                    @method($method)
                                    @csrf
                                    <input type="hidden" name="photoId" value="{{ $photo->id }}">
                                    <button type="submit" class="btn btn-link" title="{{ $photo->favoriteId ? 'Unfavorize' : 'Favorize' }}">
                                        <i class="favorize-{{ $photo->favoriteId ? 'on' : 'off' }} fa-heart"></i>
                                    </button>
                                </form>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>

        <div class="row justify-content-center">
            <div class="col-md-10">
                {{ $photos->links() }}
            </div>
        </div>
    </div>
@endsection
