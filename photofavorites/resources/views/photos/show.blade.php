@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="float-left">
                    <h2>Photo Details</h2>
                </div>
                <div class="float-right small">
                    ID {{ $photo->id }}
                </div>
            </div>
        </div>

        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card border-primary p-1 w-75">
                    <img src="{{ $photo->url }}" class="card-img-top" alt="{{ $photo->id }}">
                    <div class="card-body">
                        <h5 class="card-title">{{ $photo->title }}</h5>
                        <p class="card-text"></p>

                        <div class="float-left small position-absolute fixed-bottom p-3">ID {{ $photo->id }}</div>
                        <div class="float-right">
                            @php
                                if ($photo->favoriteId) {
                                    $action = action([\App\Http\Controllers\FavoriteController::class, 'destroy'], ['id' => $photo->favoriteId]);
                                    $method = 'DELETE';
                                } else {
                                    $action = action([\App\Http\Controllers\FavoriteController::class, 'store']);
                                    $method = 'POST';
                                }
                            @endphp

                            <form method="POST" action="{{ $action }}">
                                @method($method)
                                @csrf
                                <input type="hidden" name="photoId" value="{{ $photo->id }}">
                                <button type="submit" class="btn btn-link" title="{{ $photo->favoriteId ? 'Unfavorize' : 'Favorize' }}">
                                    <i class="favorize-{{ $photo->favoriteId ? 'on' : 'off' }} fa-heart fa-bold fa-5x"></i>
                                </button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
