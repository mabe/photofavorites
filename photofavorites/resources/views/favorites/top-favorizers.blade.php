@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="float-left">
                    <h2>Top Favorizers of the week</h2>
                </div>
            </div>
        </div>

        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="container">
                    @foreach($users as $user)
                        <div class="clearfix d-flex align-items-center">
                            <h1 class="bg-primary p-2 m-3 rounded">{{ $loop->iteration }}</h1>
                            <div class="card border-primary p-1 m-2" style="width:150px;">
                                <h4 class="card-title">{{ $user->name }}</h4>
                                <p class="card-text">
                                    favorized {{ $user->favorites_count }} photos
                                </p>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endsection
