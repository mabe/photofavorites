@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="float-left">
                    <h2>Latest favorized photos</h2>
                </div>
            </div>
        </div>

        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card-columns">
                    @foreach($favorites as $favorite)
                        <div class="card border-primary p-1" style="width:150px;">
                            <img src="{{ $favorite->getPhoto()->thumbnailUrl }}" class="card-img-top" alt="{{ $favorite->getPhoto()->id }}">
                            <div class="float-left small">{{ $favorite->getPhoto()->id }}</div>
                            <div class="float-right">
                                <i class="favorize-on fa-heart"></i> by {{ $favorite->user->name }}
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endsection
