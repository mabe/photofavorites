@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Favorites</div>

                    <div class="card-body">
                        @foreach($favorites as $favorite)
                            <div class="d-inline-block bg-primary p-2 m-2">
                                <p>FavID: {{ $favorite->id }}</p>
                                <p>UserID: {{ $favorite->user->id }}</p>
                                <p>PhotoID: {{ $favorite->getPhoto()->id ?? 'not found' }}</p>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
