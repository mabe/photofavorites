<?php

declare(strict_types=1);

use Illuminate\Database\Eloquent\Factory;
use Faker\Generator as Faker;
use App\Favorite;
use App\User;

/** @var Factory $factory */
$factory->define(Favorite::class, function (Faker $faker) {
    return [
        'photo_id' => $faker->numberBetween(1, 5000),
        'user_id' => User::inRandomOrder()->first()->id,
    ];
});
