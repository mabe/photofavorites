<?php

declare(strict_types=1);

use Illuminate\Database\Seeder;
use App\User;

class UserSeeder extends Seeder
{
    public function run(): void
    {
        factory(User::class, 17)->create();
    }
}
