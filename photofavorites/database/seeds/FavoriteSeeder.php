<?php

declare(strict_types=1);

use Illuminate\Database\Seeder;
use App\Favorite;

class FavoriteSeeder extends Seeder
{
    public function run(): void
    {
        factory(Favorite::class, 37)->create();
    }
}
