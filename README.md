# Photo-Favorites



## Requirements

- Virtualbox (or other vm)
- Vagrant



## Usage

Get files, start Homestead
```
$ git clone https://gitlab.com/mabe/photofavorites
$ cd photofavorites
$ composer install
$ vagrant up
```

Register hostname
```
$ echo "192.168.10.10 photofavorites.test" | sudo tee -a /etc/hosts
```

Connect to vm
```
$ vagrant ssh
```

Inside the vm, setup laravel
```
$ cd ~/code/photofavorites/
$ composer install
$ yarn

$ cp .env.example .env
$ php artisan key:generate
$ php artisan migrate
$ php artisan db:seed
```

Check in your browser
```
http://photofavorites.test
```



## Challenge

### Instructions

This test will take an **average duration of 2h 45min** to be done, the timing
will start once you perform your first commit to a repository and you won't be
evaluated by the amount of time spent on specific tasks but by the following
items:

- Code quality and readability
- Knowledge of the framework(s)
- Approach taken to resolve the test


### Requirements

Use Laravel and VueJS (whichever version you prefer) to create a system where
the registered users can favorite/unfavorite photos and the latest favorite
photos picked by all registered users are shown in a page accessible only by
guest users - on saturdays and sundays, instead of showing the favorited images
list for guests, a list of the users that have favorited the most in that week
is shown. Registered users can use
[JSONPlaceholder Image API](http://jsonplaceholder.typicode.com/photos)
to list all the possible images which the registered users can pick. Use a
simple pagination to go through all the possible images. More information for
JSONPlaceholder APIs can be found
[here](http://jsonplaceholder.typicode.com/).

**Tip:** JSONPlaceholder API endpoints accept the following query
parameters: \_start and \_limit

The candidate might use any other front-end plugin/tool you feel useful to
reach the desired result. The candidate may as well create tests, use design
patterns and anything he believe will enrich his showcase.

**Only applications published in a Git repository will be taken in
consideration.**
You might publish it in a private repository (Bitbucket offer free private
repositories) and give access to the following user: dev@studydrive.net

If you feel the need of sharing your environment variables - you might add the
.env to your repository , just for this test purpose.
**You won't be penalized by doing so.**


### Before you start...

Take your time, grab yourself a coffee but most importantly, relax - you can
use all the documentation and support material you want. Setup your project
within the environment your prefer, install all the necessary dependencies,
link your project to a Git repository and etc.
